<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $role = auth()->user()->role;
            if ($role === '0') {
                return redirect('/admin');
            } elseif ($role === '1') {
                return redirect('/jury');
            } elseif ($role === '2') {
                return redirect('/photographer');
            } elseif ($role === '3') {
                return redirect('/guest');
            }
        }

        return $next($request);
    }
}
