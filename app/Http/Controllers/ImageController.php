<?php

namespace App\Http\Controllers;

use App\ImageCategory;
use App\Image;
use App\Notification;
use App\Rating;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if ($user->role === '2') {
            return $user->images()->with(['imageCategory', 'ratings.user'])->get();
        } elseif ($user->role === '1') {
            $imgs = Image::with('ratings')->get()->filter(function ($value) {
                return Rating::where('image_id', $value->id)
                    ->where('user_id', auth()->user()->id)->count() === 0;
            })->sortBy(function ($image) {
                return $image->ratings->count();
            });
            return $imgs->values()->all();
        } elseif ($user->role === '3') {
            // TODO: FINISH!
        } else abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->images()->count() == 10) {
            return response(['error' => 'Elérte a maximálisan feltölthető képek számát (10 darab)!'], 422);
        }
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg',
            'image_category_id' => 'required|exists:image_categories,id',
        ]);
        if ($request->hasFile('image')) {
            if ($request->file('image')->getSize() > 1024 * 1024 * 3) {
                return response(['error' => 'Túl nagy (>3MB)'], 422);
            }
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/image');
            $file = $image->move($destinationPath, $name);

            $thumb = $this->getImageManager()->make('./image/'.$file->getFilename());
            $thumb->fit(400);
            $thumb->save('./thumb/'.$file->getFilename());
            

            $img = new Image([
                'location' => './image/'.$file->getFilename(),
                'thumbnail' => './thumb/'.$file->getFilename(),
                'image_category_id' => $request->image_category_id,
            ]);
            $img->user_id = auth()->user()->id;
            $img->file_size = filesize('./image/'.$file->getFilename());
            $img->save();

            return $img;
        } else {
            abort(422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->user();
        $img = Image::findOrFail($id);
        if ($user->role === '2') {
            if ($user->id != $img->user_id) abort(403);
        } elseif ($user->role === '1') {
            $notif = new Notification(['content' => 'Az ön egyik képét kizárta egy zsűritag a versenyből']);
            $notif->user_id = $user->id;
            $notif->save();
        }
        $img->ratings()->delete();
        $img->delete();
    }

    public function chooseImage($id) {
        $user = auth()->user();
        if (Rating::where('user_id', $user->id)->where('image_id', $id)->count() > 0) {
            return response(['error' => 'Ezt a képet már egyszer értékelted'], 422);
        }
        $rating = new Rating([
            'rating' => 1,
        ]);
        $rating->user_id = $user->id;
        $rating->image_id = Image::findOrFail($id)->id;
        $rating->save();
    }

    public function getLogo(Request $request) {
        return auth()->user()->logo;
    }

    public function uploadLogo(Request $request) {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg',
        ]);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $logo = $this->getImageManager()->make($image->getRealPath());
            if ($logo->width() === 64 && $logo->height() === 64) {
                $user = auth()->user();
                $name = $user->username.'.'.$image->getClientOriginalExtension();
                $logo->save(public_path('/logo').'/'.$name);
                $user->logo = './logo/'.$name;
                $user->save();
                return $user->logo;
            } else {
                return response(['error' => 'Csak 64x64 pixeles logót tölthet fel'], 422);
            }
        }
    }

    public function results() {
        $cats = [];
        foreach (ImageCategory::all() as $cat) {
            $cat->images = $this->calculatePoints($cat->images()->get());
            array_push($cats, $cat);
        }
        return [
            'overall' => $this->calculatePoints(Image::all()),
            'categories' => $cats,
        ];
    }

    public function listImagesByPhotographer($id) {
        return \App\User::findOrFail($id)->images()->with('user')->get();
    }
}
