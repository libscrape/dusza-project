<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function index() {
        return Config::all();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key)
    {
        $cnf = Config::where('key', $key)->firstOrFail();
        $cnf->value = $request->value;
        $cnf->save();
        return $cnf;
    }
}
