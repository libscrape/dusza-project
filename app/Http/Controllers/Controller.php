<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getImageManager() {
        return new \Intervention\Image\ImageManager(['driver' => 'gd']);
    }

    public function calculatePoints($images) {
        $imagePoints = [];
        foreach ($images as $image) {
            $points = 0;
            foreach($image->ratings as $rating) {
                $points += $rating->user()->first()->ratings()->count() - ($rating->rating - 1);
            }
            $image->points = $points;
            array_push($imagePoints, $image);
        }
        usort($imagePoints, function ($img1, $img2) {
            return $img2->points - $img1->points;
        });
        return $imagePoints;
    }
}
