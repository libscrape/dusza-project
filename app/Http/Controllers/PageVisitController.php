<?php

namespace App\Http\Controllers;

use App\PageVisit;
use Illuminate\Http\Request;

class PageVisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageVisit  $pageVisit
     * @return \Illuminate\Http\Response
     */
    public function show(PageVisit $pageVisit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageVisit  $pageVisit
     * @return \Illuminate\Http\Response
     */
    public function edit(PageVisit $pageVisit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageVisit  $pageVisit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageVisit $pageVisit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageVisit  $pageVisit
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageVisit $pageVisit)
    {
        //
    }
}
