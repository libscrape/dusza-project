<?php

namespace App\Http\Controllers;

use App\SponsorImage;
use Illuminate\Http\Request;

class SponsorImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SponsorImage::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg',
        ]);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/sponsorimgs');
            $file = $image->move($destinationPath, $name);
            $img = new SponsorImage(['location' => './sponsorimgs/'.$file->getFilename()]);
            $img->save();
            return $img;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SponsorImage  $sponsorImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SponsorImage $sponsorImage)
    {
        unlink($sponsorImage->location);
        $sponsorImage->delete();
    }
}
