<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->ratings()->with('image')->orderBy('rating')->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        foreach ($request->ratings as $id => $value) {
            $rating = Rating::findOrFail($id);
            if ($rating->user_id != auth()->user()->id) abort(403);
            $rating->rating = $value;
            $rating->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rating = Rating::findOrFail($id);
        if ($rating->user_id == auth()->user()->id) {
            $rating->delete();
        } else abort(403);
    }
}
