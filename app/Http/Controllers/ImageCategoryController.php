<?php

namespace App\Http\Controllers;

use App\ImageCategory;
use Illuminate\Http\Request;

class ImageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ImageCategory::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $cat = new ImageCategory(['name' => $request->name]);
        $cat->save();
        return $cat;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $imageCategory = ImageCategory::findOrFail($id);
        $imageCategory->name = $request->name;
        $imageCategory->save();
        return $imageCategory;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageCategory  $imageCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $imgcat = ImageCategory::findOrFail($id);
        $imgcat->delete();
    }
}
