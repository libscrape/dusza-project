<?php

namespace App\Http\Controllers;

use App\ImageLike;
use Illuminate\Http\Request;

class ImageLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImageLike  $imageLike
     * @return \Illuminate\Http\Response
     */
    public function show(ImageLike $imageLike)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImageLike  $imageLike
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageLike $imageLike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageLike  $imageLike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageLike $imageLike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageLike  $imageLike
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageLike $imageLike)
    {
        //
    }
}
