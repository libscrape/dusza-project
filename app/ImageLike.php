<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageLike extends Model
{
    private function user() {
        return $this->belongsTo('App\User');
    }
}
