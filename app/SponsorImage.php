<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SponsorImage extends Model
{
    protected $fillable = ['location'];
}
