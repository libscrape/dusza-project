<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['location', 'thumbnail', 'image_category_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function imageCategory() {
        return $this->belongsTo('App\ImageCategory');
    }

    public function ratings() {
        return $this->hasMany('App\Rating');
    }
}
