<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('./login');
});

Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);

// Auth::routes();

Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get('/', function () {
        return view('admin');
    });

    Route::resource('/users', 'UserController', ['only' => ['index', 'store', 'destroy']]);
    Route::resource('/sponsor-images', 'SponsorImageController', ['only' => ['index', 'store', 'destroy']]);
    Route::resource('/categories', 'ImageCategoryController', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('/configs', 'ConfigController', ['only' => ['index', 'update']]);
    Route::get('/images/results', 'ImageController@results');
});

Route::prefix('photographer')->middleware('photographer')->group(function () {
    Route::get('/', function () {
        return view('photographer');
    });

    Route::resource('/categories', 'ImageCategoryController', ['only' => ['index']]);
    Route::resource('/images', 'ImageController', ['only' => ['index', 'store', 'destroy']]);
    Route::get('/logo', 'ImageController@getLogo');
    Route::post('/logo', 'ImageController@uploadLogo');
});

Route::prefix('guest')->middleware('vendeg')->group(function () {
    Route::get('/', function () {
        return view('guest');
    });

    Route::get('/photographers', 'UserController@listPhotographers');
    Route::get('/photographers/{id}', 'ImageController@listImagesByPhotographer');
});

Route::prefix('jury')->middleware('jury')->group(function () {
    Route::get('/', function () {
        return view('jury');
    });

    Route::resource('/images', 'ImageController', ['only' => ['index', 'destroy']]);
    Route::post('/images/{id}/choose', 'ImageController@chooseImage');
    Route::resource('/ratings', 'RatingController', ['only' => ['index', 'destroy']]);
    Route::post('/ratings', 'RatingController@update');
});