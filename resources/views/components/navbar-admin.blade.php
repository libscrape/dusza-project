<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#adminNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand justify-content-end" href="#" style="pointer-events:none"><h4 class="mb-0">ADMIN</h4></a>
    
    <div class="collapse navbar-collapse" id="adminNavbar">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
            <a class="nav-link" :class="{ active: currentPage === 'UsersPage' }" href="#UsersPage">
                <i class="fas fa-users"></i>Felhasználókezelés
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" :class="{ active: currentPage === 'CategoriesPage' }" href="#CategoriesPage">
                <i class="fas fa-plus-square"></i>Témakörök módosítása
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#StatPage" id="navbarDropdownMenuLink" data-toggle="dropdown">
                <i class="fas fa-poll"></i>Statisztika
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" id="topdrop" href="#StatPage">Diagramok</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#StatPage">Látogatók</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" id="botdrop" href="#StatPage">Zsűritagok </a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#StatePage" :class="{ active: currentPage === 'StatePage' }">
                <i class="fas fa-list-ol"></i>A verseny állása
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#SponsorPage" :class="{ active: currentPage === 'SponsorPage' }" >
                <i class="fas fa-file-upload"></i>Szponzorkép feltöltése
            </a>
        </li>
        <li class="nav-item">
            <a href="javascript:document.getElementById('logoutForm').submit()" class="nav-link">
                <i class="fas fa-sign-out-alt"></i>Kijelentkezés
            </a>
            <form class="d-none" id="logoutForm" method="POST" action="./logout">{{csrf_field()}}</form>
        </li>
        </ul>
    </div>
</nav>