<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#adminNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand justify-content-end" href="#" style="pointer-events:none"><h4 class="mb-0">Zsűri</h4></a>
    
    <div class="collapse navbar-collapse" id="adminNavbar">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
            <a class="nav-link" :class="{ active: currentPage === 'InterestPage' }" href="#InterestPage">
                <i class="fas fa-plus-square"></i>Új képek
            </a>
        </li>
        
        <li class="nav-item">
            <a class="nav-link" href="#RatingPage" :class="{ active: currentPage === 'RatingPage' }">
                <i class="fas fa-star"></i>Értékes képek
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" :class="{ active: currentPage === 'LogoUpPage' }" href="#">
            <i class="fas fa-flag-checkered"></i>Verseny végeredménye
            </a>
        </li>

        <li class="nav-item">
            <a href="javascript:document.getElementById('logoutForm').submit()" class="nav-link">
                <i class="fas fa-sign-out-alt"></i>Kijelentkezés
            </a>
            <form class="d-none" id="logoutForm" method="POST" action="./logout">{{csrf_field()}}</form>
        </li>
        </ul>
    </div>
</nav>