<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#guestNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand justify-content-end" href="#" style="pointer-events:none"><h4 class="mb-0">Vendég</h4></a>
    
    <div class="collapse navbar-collapse" id="guestNavbar">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" :class="{ active: currentPage === 'GalleryPage' }" href="#GalleryPage">
                <i class="fas fa-images"></i>Nevezett képek megtekintése</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" :class="{ active: currentPage === 'FinishPage' }" href="#FinishPage">
                <i class="fas fa-flag-checkered"></i>Verseny végeredménye
                </a>
            </li>

            <li class="nav-item">
                <a href="javascript:document.getElementById('logoutForm').submit()" class="nav-link">
                    <i class="fas fa-sign-out-alt"></i>Kijelentkezés
                </a>
                <form class="d-none" id="logoutForm" method="POST" action="./logout">{{csrf_field()}}</form>
            </li>

        </ul>
    </div>
</nav>