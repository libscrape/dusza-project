<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#photographerNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand justify-content-end" href="#" style="pointer-events:none"><h4 class="mb-0">Fotós</h4></a>
    
    <div class="collapse navbar-collapse" id="photographerNavbar">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" :class="{ active: currentPage === 'SelfImagesPage' }" href="#SelfImagesPage">
                <i class="fas fa-images"></i>Saját képeim
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" :class="{ active: currentPage === 'LogoUpPage' }" href="#LogoUpPage">
                <i class="fas fa-plus-square"></i>Logó feltöltése
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" :class="{ active: currentPage === 'LogoUpPage' }" href="#">
                <i class="fas fa-flag-checkered"></i>Verseny végeredménye
                </a>
            </li>


            
            
            <li class="nav-item">
                <a href="javascript:document.getElementById('logoutForm').submit()" class="nav-link">
                    <i class="fas fa-sign-out-alt"></i>Kijelentkezés
                </a>
                <form class="d-none" id="logoutForm" method="POST" action="./logout">{{csrf_field()}}</form>
            </li>

        </ul>
    </div>
</nav>