@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="./css/login.css">
@endsection

@section('content')
<!-- login-->
<div class="modal-dialog text-center" > 
	<div class="col-sm-8 main-section">
	  <div class="modal-content">
            <div class="col-12 user-img">
                <img src="face.PNG">
            </div>

            <form class="col-12" method="POST" autocomplete="off">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Felhasználónév">
                </div>
                <div class="form-group" id="password">
                    <input type="password" name="password" class="form-control" placeholder="Jelszó">
                </div>
                <div class="form-group" id="password">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Jelszó megerősítése">
                </div>
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{$error}}</div>
                @endforeach
                
                <div class="d-flex flex-column">
                    <button type="submit" class="btn mb-4"><i class="fas fa-user-plus"></i>&nbsp;Regisztráció</a>
                </div>
            </form>
	   </div>
    </div>
</div>

@endsection
