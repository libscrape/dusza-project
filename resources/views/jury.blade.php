@extends('layouts.app')

@section('head')
    <script defer src="{{ asset('./js/jury.js') }}"></script>
@endsection

@section('content')
    @include('components.navbar-jury')
    <div class="container mt-4">
        <div :is="currentPage"></div>
    </div>
@endsection
