@extends('layouts.app')

@section('head')
    <script defer src="{{ asset('./js/admin.js') }}"></script>
@endsection

@section('content')
    @include('components.navbar-admin')
    <div class="container mt-4">
        <div :is="currentPage"></div>
    </div>
@endsection
