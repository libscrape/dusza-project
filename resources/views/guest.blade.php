@extends('layouts.app')

@section('head')
    <script defer src="{{ asset('./js/guest.js') }}"></script>
@endsection

@section('content')
    @include('components.navbar-guest')
    <div class="container mt-4">
        <div :is="currentPage"></div>
    </div>
@endsection
