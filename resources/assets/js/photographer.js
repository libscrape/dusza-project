import axios from 'axios';
import Noty from 'noty';
import Vue from 'vue';

import SelfImagesPage from './pages/SelfImagesPage.vue';
import LogoUpPage from './pages/LogoUpPage.vue';
import RatingsPage from './pages/RatingsPage.vue';

window.fetchApi = (method, url, data, cb) => {
    axios({
        method,
        url,
        data
    }).then((resp) => {
        if (resp.data.error !== undefined) {
            new Noty({ text: resp.data.error, type: 'error', timeout: 2000, layout: 'bottomLeft' }).show();
        } else {
            if (method.toUpperCase() !== 'GET') new Noty({ text: 'Sikeres!', type: 'success', timeout: 3000, layout: 'bottomLeft' }).show();
            cb(resp.data);
        }
    }).catch((e) => {
        if (e.response) {
            let res = '';
            for (const key in e.response.data) {
                if (e.response.data.hasOwnProperty(key)) {
                    const elm = e.response.data[key];
                    res += elm + "\n";
                }
            }
            new Noty({
                text: res,
                timeout: 2000,
                type: 'error',
                layout: 'bottomLeft'
            }).show();
        } else new Noty({ text: e.message, type: 'error', timeout: 2000, layout: 'bottomLeft' }).show();
    });
};

const vm = new Vue({
    el: '#app',
    components: {
        SelfImagesPage,
        LogoUpPage,
        RatingsPage,
    },
    data: {
        currentPage: null,
    },
    beforeMount() {
        this.routeHash();
    },
    methods: {
        routeHash() {
            const compl = window.location.hash.substring(1);
            const base = compl.indexOf('?') !== -1 ? compl.substring(0, compl.indexOf('?')) : compl;
            window.urlParams = compl.indexOf('?') !== -1
                ? compl.substring(compl.indexOf('?') + 1).split('&').map(v => v.split('=')).reduce((pre, [k, v]) => ({...pre, [k]: v}), {})
                : {};

            if (this.$options.components[base] === undefined) {
                window.location.hash = '#SelfImagesPage';
            } else {
                this.currentPage = base;
            }
        }
    }
});

window.onhashchange = () => {
    vm.routeHash();
};