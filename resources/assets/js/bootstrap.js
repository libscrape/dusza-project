window.$ = window.jQuery = require('jquery');
import 'bootstrap';
import '@fortawesome/fontawesome-free';

window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};
