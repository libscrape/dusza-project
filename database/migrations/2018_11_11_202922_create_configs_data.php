<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('configs')->insert([
            'key' => 'is_running',
            'value' => 'false',
            'created_at' => time(),
        ]);
        DB::table('configs')->insert([
            'key' => 'image_visibility',
            'value' => '180',
            'created_at' => time(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('configs')->where('key', 'is_runnning')->delete();
        DB::table('configs')->where('key', 'image_visibility')->delete();
    }
}
